#include <queue>
#include "dependency_graph.h"

set<size_t> DependencyGraph::get_relevent_vars(vector<size_t>& changed_vars){
	queue<size_t> q;
	set<size_t> s;
	for(auto var: changed_vars)
		q.push(var);

	while(!q.empty()){
		auto var = q.front(); q.pop();
		if(s.find(var) != s.end())
			continue;
		else
			s.insert(var);
		
		for(auto& e: get_outgoing(var)){
			q.push(e->dest);
		}
	}
	return s;
}
