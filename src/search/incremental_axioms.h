#ifndef INCREMENTAL_AXIOMS_H
#define INCREMENTAL_AXIOMS_H
#include <iostream>
#include "axioms.h"
using namespace std;

class IncrementalAxiomEvaluator : public AxiomEvaluator {
public:
    explicit IncrementalAxiomEvaluator(const TaskProxy &task_proxy);
};
#endif
