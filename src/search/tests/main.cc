#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <vector>
#include <memory>
#include <set>
#include "catch.hpp"
#include "graph.h"

TEST_CASE( "DependencyGraph.get_relevent_vars", "[dependency_graph]" ) {
	DependencyGraph g(5);
	g.add_edge(unique_ptr<Edge>(new Edge(0,1)));
	g.add_edge(unique_ptr<Edge>(new Edge(0,2)));
	g.add_edge(unique_ptr<Edge>(new Edge(2,0)));
	g.add_edge(unique_ptr<Edge>(new Edge(3,4)));

    SECTION( "Given query = {0} returns {0,1,2}" ) {
		vector<size_t> query = {0};
		set<size_t> expected = {0,1,2};

        REQUIRE( g.get_relevent_vars(query) == expected );
    }

    SECTION( "Given query = {2} returns {0,1,2}" ) {
		vector<size_t> query = {0};
		set<size_t> expected = {0,1,2};

        REQUIRE( g.get_relevent_vars(query) == expected );
    }

    SECTION( "Given query = {} returns {}" ) {
		vector<size_t> query = {};
		set<size_t> expected = {};

        REQUIRE( g.get_relevent_vars(query) == expected );
    }

    SECTION( "Given query = {3} returns {3,4}" ) {
		vector<size_t> query = {3};
		set<size_t> expected = {3,4};

        REQUIRE( g.get_relevent_vars(query) == expected );
    }
}

