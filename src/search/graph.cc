#include "graph.h"

void Graph::add_edge(std::unique_ptr<Edge>&& e){
	adj_lst[e->src].push_back(e.get());
	rev_adj_lst[e->dest].push_back(e.get());
	edges.push_back(move(e));
}

vector<Edge*>& Graph::get_outgoing(int index){
	return adj_lst[index];
}
vector<Edge*>& Graph::get_incoming(int index){
	return rev_adj_lst[index];
}

void Graph::dump_edges() {
	for(auto& edge: edges) {
		cout << edge->src << " -> " << edge->dest << endl;
	}
}
