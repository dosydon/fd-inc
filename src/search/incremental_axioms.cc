#include "incremental_axioms.h"
#include "utils/timer.h"

#include "int_packer.h"
#include "task_tools.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>
using namespace std;

IncrementalAxiomEvaluator::IncrementalAxiomEvaluator(const TaskProxy &task_proxy): AxiomEvaluator(task_proxy){
}
