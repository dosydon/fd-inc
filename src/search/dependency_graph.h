#pragma once
#include "graph.h"

class DependencyGraph: public Graph {
public:
	explicit DependencyGraph():Graph(){};
	explicit DependencyGraph(size_t n):Graph(n){};
	set<size_t> get_relevent_vars(vector<size_t>& changed_vars);
};
