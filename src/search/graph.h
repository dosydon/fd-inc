#pragma once
#include <cstdlib>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <cassert>
#include <memory>
#include <set>
using namespace std;

class Edge{
public:
	int src,dest;
	Edge(int src, int dest): src(src),dest(dest){};
};

class Graph{
private:
	size_t n;
public:
	explicit Graph(){};
	explicit Graph(size_t n):n(n),adj_lst(n),rev_adj_lst(n){};
	vector<std::unique_ptr<Edge>> edges;
	vector<vector<Edge*>> adj_lst;
	vector<vector<Edge*>> rev_adj_lst;
	vector<Edge*>& get_outgoing(int index);
	vector<Edge*>& get_incoming(int index);

	virtual void add_edge(unique_ptr<Edge>&& e);
	size_t size()const {return n;};
	void dump_edges();
};
