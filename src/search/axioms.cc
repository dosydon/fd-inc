#include "axioms.h"
#include "utils/timer.h"

#include "int_packer.h"
#include "task_tools.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

using namespace std;

AxiomEvaluator::AxiomEvaluator(const TaskProxy &task_proxy) {
	total_time = 0;

    task_has_axioms = has_axioms(task_proxy);
    if (task_has_axioms) {
        VariablesProxy variables = task_proxy.get_variables();
        AxiomsProxy axioms = task_proxy.get_axioms();

        // Initialize literals
        for (VariableProxy var : variables)
            axiom_literals.emplace_back(var.get_domain_size());

        // Initialize rules
        for (OperatorProxy axiom : axioms) {
            assert(axiom.get_effects().size() == 1);
            EffectProxy cond_effect = axiom.get_effects()[0];
            FactPair effect = cond_effect.get_fact().get_pair();
            int num_conditions = cond_effect.get_conditions().size();
            AxiomLiteral *eff_literal = &axiom_literals[effect.var][effect.value];
            rules.emplace_back(
                num_conditions, effect.var, effect.value, eff_literal);
        }

        // Cross-reference rules and literals
        for (OperatorProxy axiom : axioms) {
            EffectProxy effect = axiom.get_effects()[0];
            for (FactProxy condition : effect.get_conditions()) {
                int var_id = condition.get_variable().get_id();
                int val = condition.get_value();
                AxiomRule *rule = &rules[axiom.get_id()];
                axiom_literals[var_id][val].condition_of.push_back(rule);
            }
        }

        // Initialize negation-by-failure information
        int last_layer = -1;
        for (VariableProxy var : variables) {
            if (var.is_derived()) {
                last_layer = max(last_layer, var.get_axiom_layer());
            }
        }
        nbf_info_by_layer.resize(last_layer + 1);

        for (VariableProxy var : variables) {
            if (var.is_derived()) {
                int layer = var.get_axiom_layer();
                if (layer != last_layer) {
                    int var_id = var.get_id();
                    int nbf_value = var.get_default_axiom_value();
                    AxiomLiteral *nbf_literal = &axiom_literals[var_id][nbf_value];
                    nbf_info_by_layer[layer].emplace_back(var_id, nbf_literal);
                }
            }
        }

        default_values.reserve(variables.size());
        for (VariableProxy var : variables) {
            if (var.is_derived())
                default_values.emplace_back(var.get_default_axiom_value());
            else
                default_values.emplace_back(-1);
        }

		// Initialize dependency_graph
		dependency_graph = unique_ptr<DependencyGraph>(new DependencyGraph(variables.size()));
        for (OperatorProxy axiom : axioms) {
            assert(axiom.get_effects().size() == 1);
            EffectProxy cond_effect = axiom.get_effects()[0];
            FactPair effect = cond_effect.get_fact().get_pair();

            for (FactProxy condition : cond_effect.get_conditions()) {
                int var_id = condition.get_variable().get_id();
				dependency_graph->add_edge(unique_ptr<Edge>(new Edge(var_id, effect.var)));
            }
        }

		dependency_graph->dump_edges();
    }
}

void AxiomEvaluator::initialize_relevent_vars(PackedStateBin *buffer, const IntPacker &state_packer, vector<size_t>& relevent_vars) {
	for(auto& var_id: relevent_vars) {
		int default_value = default_values[var_id];
		if (default_value != -1) {
			state_packer.set(buffer, var_id, default_value);
		} else {
			int value = state_packer.get(buffer, var_id);
			queue.push_back(&axiom_literals[var_id][value]);
		}
	}
}

void AxiomEvaluator::initialize_relevent_rules(PackedStateBin *buffer, const IntPacker &state_packer, std::vector<AxiomRule>& relevent_rules) {
    for (AxiomRule &rule : relevent_rules) {
        rule.unsatisfied_conditions = rule.condition_count;

        /*
          TODO: In a perfect world, trivial axioms would have been
          compiled away, and we could have the following assertion
          instead of the following block.
          assert(rule.condition_count != 0);
        */
        if (rule.condition_count == 0) {
            /*
              NOTE: This duplicates code from the main loop below.
              I don't mind because this is (hopefully!) going away
              some time.
            */
            int var_no = rule.effect_var;
            int val = rule.effect_val;
            if (state_packer.get(buffer, var_no) != val) {
                state_packer.set(buffer, var_no, val);
                queue.push_back(rule.effect_literal);
            }
        }
    }
}

// TODO rethink the way this is called: see issue348.
void AxiomEvaluator::evaluate(PackedStateBin *buffer,
                              const IntPacker &state_packer, const GlobalOperator* op) {
	if(op){
		cout << op->get_name() << endl;
		vector<size_t> vars;
		for(auto& effect: op->get_effects()){
			vars.push_back(effect.var);
		}

		auto relevent_vars = dependency_graph->get_relevent_vars(vars);

		for(auto var: relevent_vars){
			cout << var << endl;
		}

		for(auto& rule: get_relevent_rules(relevent_vars)){
			cout << rule.effect_var << ":" << rule.effect_val << endl;
		}
	}

    if (!task_has_axioms)
        return;
	utils::Timer axiom_timer;

    assert(queue.empty());

	vector<size_t> relevent_vars;
	for(size_t i = 0; i < default_values.size(); i++)
		relevent_vars.push_back(i);
	initialize_relevent_vars(buffer,state_packer,relevent_vars);

	initialize_relevent_rules(buffer,state_packer,rules);

    for (size_t layer_no = 0; layer_no < nbf_info_by_layer.size(); ++layer_no) {
        // Apply Horn rules.
        while (!queue.empty()) {
            AxiomLiteral *curr_literal = queue.back();
            queue.pop_back();
            for (size_t i = 0; i < curr_literal->condition_of.size(); ++i) {
                AxiomRule *rule = curr_literal->condition_of[i];
                if (--rule->unsatisfied_conditions == 0) {
                    int var_no = rule->effect_var;
                    int val = rule->effect_val;
                    if (state_packer.get(buffer, var_no) != val) {
                        state_packer.set(buffer, var_no, val);
                        queue.push_back(rule->effect_literal);
                    }
                }
            }
        }

        /*
          Apply negation by failure rules. Skip this in last iteration
          to save some time (see issue420, msg3058).
        */
        if (layer_no != nbf_info_by_layer.size() - 1) {
            const vector<NegationByFailureInfo> &nbf_info = nbf_info_by_layer[layer_no];
            for (size_t i = 0; i < nbf_info.size(); ++i) {
                int var_no = nbf_info[i].var_no;
                // Verify that variable is derived.
                assert(default_values[var_no] != -1);
                if (state_packer.get(buffer, var_no) == default_values[var_no])
                    queue.push_back(nbf_info[i].literal);
            }
        }
    }
	axiom_timer.stop();
	total_time += axiom_timer();
}

double AxiomEvaluator::get_total_time(){
	return total_time;
}

vector<AxiomRule> AxiomEvaluator::get_relevent_rules(std::set<size_t> relevent_vars){
	vector<AxiomRule> relevent_rules;
	for(auto& rule: rules){
		if(relevent_vars.find(rule.effect_var) != relevent_vars.end())
			relevent_rules.push_back(rule);
	}
	return relevent_rules;
}
